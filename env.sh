export ORACLE_BASE=/u01/app/oracle # Solo aplica si tienes una instalación física de Oracle
export ORACLE_HOME=$ORACLE_BASE/product/19.3.0/dbhome_1 #Igual que arriba, ignora si utilizas Instant Client
export CLASSPATH=$ORACLE_HOME/jlib:$ORACLE_HOME/rdbms/jlib #Solo aplica para instalaciones físicas

export DATABASE_DSN=ADMIN/SecurePassword1@renato_medium #String de conexión
export TNS_ADMIN=/home/renato/Oracle/Credentials # Ruta hacia tu wallet
export LD_LIBRARY_PATH=$ORACLE_HOME/lib:/lib:/usr/lib #Si tienes Instant Client, reemplaza esta ruta por la dirección en la que descomprimiste las librerías
export PATH=$PATH:$LD_LIBRARY_PATH #No olvides incluir este directorio en tu PATH!
